<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class contentController extends Controller
{
  public function index(){
    return view('content');
  }

  public function show($idContent){
    return view('singleContent', compact('idContent'));
  }
}

/**
 * use die($param); to stop program end see value of $param
 * 
 * return view('nameOfBladeFile', ['varToSend'=> $param]) or
 * return view('nameOfBladeFile', compact('param')) in blade file variabel key must be equal
 * 
 */
